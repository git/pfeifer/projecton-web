#!/usr/bin/env python

import cgi
import sys
import smtplib
import os.path
import pickle
import time
import cgitb

cgitb.enable()

sender    = 'hagen@jauu.net'
receivers = ['hagen@jauu.net']
db_path   = '/tmp/projecton-users.db'

last_ip  = list()


form = cgi.FieldStorage()
user = form.getfirst("name", "No Name")
desc = form.getfirst("description", "No Description")
ip   = cgi.escape(os.environ["REMOTE_ADDR"])


if os.path.isfile(db_path):
    db = open(db_path, 'rb')
    last_ip = pickle.load(db)
    db.close()


print "Content-type: text/html"
print ""


if ip in last_ip:
    print "<H1>Already registered from this IP {0}!</H1><br />".format(ip)
    print "<H2>Please send email to info@projecton.eu for further question</H2>"
    time.sleep(1)
    sys.exit()

# save ip but first truncate to 16 elements
last_ip = last_ip[:16]
last_ip.append(ip)



# write data
db = open(db_path,'wb')
pickle.dump(last_ip, db)
db.close()



message  = "From: Projecton Registration <hagen@jauu.net>\n"
message += "To: Hagen Paul Pfeifer <hagen@jauu.net>\n"
message += "Subject: New Projecton Registration\n\n"

message += "Name:        {0}\n".format(user)
message += "IP:          {0}\n".format(ip)
message += "Description: {0}\n".format(desc)
message += "\n\n"


try:
    smtpObj = smtplib.SMTP('localhost')
    smtpObj.sendmail(sender, receivers, message)
    print "<H1>Thank You {0}!</H1><br />".format(ip)
    print "Successfully registrated<br />"
    print "See you soon!"
except SMTPException:
    print "Error: unable to registrate! Huh? (please send email directly to info@projecton.eu)"
