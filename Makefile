BASEDIR=$(PWD)
OUTPUTDIR=$(BASEDIR)/.
CONFFILE=$(BASEDIR)/pelican.conf.py

SSH_HOST=jauu.net
SSH_USER=pfeifer
SSH_TARGET_DIR=/var/www/projecton.eu

all: sync

help:
	@echo 'Makefile for a pelican Web site                                       '
	@echo '                                                                      '
	@echo 'Usage:                                                                '
	@echo '   make html                        (re)generate the web site         '
	@echo '   make clean                       remove the generated files        '
	@echo '   sync   			                     upload the web site using SSH     '
	@echo '                                                                      '


sync: $(OUTPUTDIR)/index.html
	rsync --verbose -P --checksum  --exclude '.git' -rloD $(OUTPUTDIR)/ $(SSH_USER)@$(SSH_HOST):$(SSH_TARGET_DIR)/


.PHONY: html help clean sync
    
